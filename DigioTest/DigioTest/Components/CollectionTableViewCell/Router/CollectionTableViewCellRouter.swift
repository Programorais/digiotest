//
//  CollectionTableViewCellRouter.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 11/11/20.
//

import UIKit

extension UITableView {
    func dequeueReusableCollectionTableViewCell(data: [MainPageData], cellType: CellType, indexPath: IndexPath) -> CollectionTableViewCell? {
        
        let characterTableViewCell =  self.dequeueReusableCell(withIdentifier: "CollectionTableViewCell", for: indexPath) as? CollectionTableViewCell
        characterTableViewCell?.configure(data: data, cellType: cellType)
        return characterTableViewCell
    }
    
}

