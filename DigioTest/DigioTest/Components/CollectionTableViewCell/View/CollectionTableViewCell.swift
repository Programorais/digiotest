//
//  CollectionTableViewCell.swift
//  DigioTest
//
//  Created by Vinicios de Morais on 16/11/20.
//

import UIKit

final class CollectionTableViewCell: UITableViewCell {
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    var data = [MainPageData](){
        didSet{
            collectionView.reloadData()
        }
    }
    
    var cellType =  CellType.product
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupViewCode()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: [MainPageData], cellType: CellType){
        self.data = data
        self.cellType = cellType
    }
    
}

extension CollectionTableViewCell: ViewCodeProtocol {
    func configureViews() {
        
        selectionStyle = .none
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "UICollectionViewCell")
        collectionView.reloadData()
    }
    
    func addSubviews() {
        self.contentView.addSubview(collectionView)
    }
    
    func setupConstraints() {
        self.setupConstraintsToBounds(view: self.collectionView, parentView: self.contentView, paddingAround: 0)
    }
}

extension CollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UICollectionViewCell", for: indexPath)
        
        let imageView = UIImageView(image: self.data[indexPath.row].image)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        cell.contentView.addSubview(imageView)

        self.setupConstraintsToBounds(view: imageView,
                                      parentView: cell.contentView,
                                      paddingAround: (self.cellType == CellType.product ? 10 : 0))

        if self.cellType == CellType.product {
            cell.contentView.layer.cornerRadius = 12
            cell.contentView.layer.borderWidth = 0.1
            cell.contentView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
        } else {
            imageView.layer.cornerRadius = 12
            imageView.layer.borderWidth = 0.1
            imageView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
        }
        
        
        
        return cell
    }
    
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        switch self.cellType {
        case .cash:
            return CGSize(width: 380, height: collectionView.frame.height)
        case .product:
            return CGSize(width: 80, height: collectionView.frame.height)
        case .spotlight:
            return CGSize(width: 380, height: collectionView.frame.height)
        }
    }
}



enum CellType: Int {
    case product = 2
    case cash = 1
    case spotlight = 0
}

