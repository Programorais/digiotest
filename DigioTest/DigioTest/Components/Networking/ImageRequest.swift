//
//  ImageRequest.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation
import UIKit

class ImageRequest {
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
}
 
extension ImageRequest: NetworkProtocol {
    func decode(_ data: Data) -> UIImage? {
        return UIImage(data: data)
    }
    
    func request(withCompletion completion: @escaping (UIImage?) -> Void) {
        request(url, withCompletion: completion)
    }
}
