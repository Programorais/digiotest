//
//  APIResource.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation

protocol ApiResource {
    associatedtype ModelType: Codable
    var methodPath: String { get }
    var urlString: String { get }
    var querryItems: [URLQueryItem] {get}
}
 
extension ApiResource {

    var url: URL? {
        var components = URLComponents(string: urlString)
        components?.path = methodPath
        components?.queryItems = querryItems
        return components?.url
    }
}
