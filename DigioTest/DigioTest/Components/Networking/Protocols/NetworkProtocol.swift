//
//  NetworkProtocol.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation


protocol NetworkProtocol: AnyObject {
    associatedtype ModelType
    func decode(_ data: Data) -> ModelType?
    func request(withCompletion completion: @escaping (ModelType?) -> Void)
}

extension NetworkProtocol {
    func request(_ url: URL, withCompletion completion: @escaping (ModelType?) -> Void) {
            let session = URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
            let task = session.dataTask(with: url, completionHandler: {  (data: Data?, response: URLResponse?, error: Error?) -> Void in
                guard let data = data else {
                    completion(nil)
                    return
                }
                print(data.prettyPrintedJSONString ?? "")
                completion(self.decode(data))
            })
            task.resume()
        }
}

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
