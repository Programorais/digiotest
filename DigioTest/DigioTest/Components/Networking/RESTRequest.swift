//
//  RESTRequest.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation

class RESTRequest<T: ApiResource> {
    let resource: T
    
    init(resource: T) {
        self.resource = resource
    }
}
 
extension RESTRequest: NetworkProtocol {
    func request(withCompletion completion: @escaping (T.ModelType?) -> Void) {
        guard let validURL = resource.url else {return}
        request(validURL, withCompletion: completion)
    }
    
    func decode(_ data: Data) -> T.ModelType? {
        let wrapper = try? JSONDecoder().decode(T.ModelType.self, from: data)
        return wrapper
    }
    
    
}
