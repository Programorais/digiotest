//
//  SectionData.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import Foundation

struct SectionData<T> {
    var title: NSAttributedString?
    var items: [T]
    
    mutating func append(item: T) {
        self.items.append(item)
    }
}
