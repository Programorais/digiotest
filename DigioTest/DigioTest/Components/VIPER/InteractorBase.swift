//
//  InteractorBase.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//



protocol BaseInteractorProtocol: AnyObject {
}

class BaseInteractor<T>: BaseInteractorProtocol {
    
    private weak var baseDelegate: BaseInteractorDelegate?
    
    var delegate: T? {
        get {
            return self.baseDelegate as? T
        }
    }
    
    init(delegate: BaseInteractorDelegate?) {
        self.baseDelegate = delegate
    }
}

protocol BaseInteractorDelegate: AnyObject {
}
