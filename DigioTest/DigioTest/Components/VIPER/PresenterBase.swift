//
//  PresenterBase.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import Foundation

protocol BasePresenterProtocol: AnyObject {
    func viewDidLoad()
    func setUp(interactor: BaseInteractorProtocol)
    
}

class BasePresenter<T, V, Z>: BasePresenterProtocol {
    private weak var baseDelegate: BasePresenterDelegate?
    private var baseRouter: BaseRouterProtocol?
    private var baseInteractor: BaseInteractorProtocol?
    
    
    var delegate: T? {
        get {
            return self.baseDelegate as? T
        }
    }
    
    var router: V? {
        get {
            return self.baseRouter as? V
        }
    }
    
    var interactor: Z? {
        get {
            return self.baseInteractor as? Z
        }
    }
    
    init(delegate: BasePresenterDelegate?, router: BaseRouterProtocol?) {
        self.baseDelegate = delegate
        self.baseRouter = router
    }
    
    func setUp(interactor: BaseInteractorProtocol) {
        self.baseInteractor = interactor
    }
    
    func viewDidLoad() {
    }
}

protocol BasePresenterDelegate: AnyObject {
    
}
