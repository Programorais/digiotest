//
//  ViewBase.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import UIKit

protocol BaseViewControllerProtocol: AnyObject {
    func setUp(presenter: BasePresenterProtocol)
}

class BaseViewController<T>: UIViewController, BaseViewControllerProtocol {
    
    private var basePresenter: BasePresenterProtocol?
    

    var presenter: T? {
        get {
            return self.basePresenter as? T
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basePresenter?.viewDidLoad()
    }
    
    
    func setUp(presenter: BasePresenterProtocol) {
        self.basePresenter = presenter
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }

}
