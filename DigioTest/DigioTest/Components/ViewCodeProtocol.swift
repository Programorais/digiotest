//
//  ViewCode.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 11/11/20.
//

import UIKit

protocol ViewCodeProtocol {
    func configureViews()
    func addSubviews()
    func setupConstraints()
    func setupConstraintsToBounds(view: UIView, parentView: UIView, paddingAround: CGFloat)
}


extension ViewCodeProtocol {
    
    func setupViewCode() {
        self.addSubviews()
        self.setupConstraints()
        self.configureViews()
    }
    
    func setupConstraintsToBounds(view: UIView, parentView: UIView, paddingAround: CGFloat) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: -paddingAround).isActive = true
        view.topAnchor.constraint(equalTo: parentView.topAnchor, constant: paddingAround).isActive = true
        view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: paddingAround).isActive = true
        view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: -paddingAround).isActive = true
    }
    
}
