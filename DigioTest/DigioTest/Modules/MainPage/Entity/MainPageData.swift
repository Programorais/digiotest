//
//  CharacterData.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 15/11/20.
//

import UIKit

class MainPageData {
    
    internal init(name: String, description: String, image: UIImage) {
        self.name = name
        self.description = description
        self.image = image
    }
    
    let name: String
    let description: String
    let image: UIImage

}
