//
//  MainPageInteractor.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import UIKit
import Foundation


protocol MainPageInteractorProtocol {
    func loadList()
}

final class MainPageInteractor: BaseInteractor<MainPageInteractorDelegate> {
    
    let service: ScreenSetupService
    
    init(delegate: MainPageInteractorDelegate?, service: ScreenSetupService) {
        self.service = service
        super.init(delegate: delegate)
        
    }
}

extension MainPageInteractor: MainPageInteractorProtocol {
       
    func loadList() {
        service.getScreenSetup { [weak self] (setupInfo) in
            guard let self = self,
                  let setupInfo = setupInfo else { return }
            
            self.service.getImages(setup: setupInfo) { [weak self] (images) in
                
                guard let self = self else {return}
                
                let sectionData = self.mapMainPage(setup: setupInfo, images: images).enumerated().map { (index, data) -> SectionData<MainPageData> in
                    
                    var title = NSAttributedString(string: "")
                    let blueAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1919241548, green: 0.2187272608, blue: 0.309043467, alpha: 1),
                                          NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)] as [NSAttributedString.Key : Any]
                    
                    
                    if index == 1 {
                        let grayAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)]
                        
                        let firstString = NSMutableAttributedString(string: "digio ", attributes: blueAttributes)
                        let secondString = NSAttributedString(string: "Cash", attributes: grayAttributes)

                        firstString.append(secondString)
                        title = firstString
                    }
                    if index == 2 {
                        let attString = NSMutableAttributedString(string: "Produtos", attributes: blueAttributes)
                        title = attString
                        
                    }
                    
                    return SectionData<MainPageData>(title: title, items: data)
                }
                
                self.delegate?.didLoadMainPage(screenSetup: sectionData)
            }
            
            
        }
    }
    
    func mapMainPage(setup: ScreenSetup, images: [[UIImage]]) -> [[MainPageData]] {
        
        var mainData = [[MainPageData]]()
        
        
        let spotlight = setup.spotlight.enumerated().map { (index, spotlight) -> MainPageData in
            return MainPageData(name: spotlight.name,
                                description: spotlight.description,
                                image: images[0][index])
        }
        
        let cash = MainPageData(name: setup.cash.title,
                                description: setup.cash.description,
                                image: images[1][0])
        
        let products = setup.products.enumerated().map { (index, product) -> MainPageData in
            return MainPageData(name: product.name,
                                description: product.description,
                                image: images[2][index])
        }
        
        mainData.append(spotlight)
        mainData.append([cash])
        mainData.append(products)
        return mainData
    }
}


protocol MainPageInteractorDelegate: BaseInteractorDelegate {
    func didLoadMainPage(screenSetup: [SectionData<MainPageData>])
}
