//
//  MainPagePresenter.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import Foundation


protocol MainPagePresenterProtocol: BasePresenterProtocol {
  
}


final class MainPagePresenter: BasePresenter<MainPagePresenterDelegate, MainPageRouterProtocol, MainPageInteractorProtocol> {
    
    private var data = [SectionData<MainPageData>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.loadList()
    }
    
}

protocol MainPagePresenterDelegate: BasePresenterDelegate {
    func updateMainPage(data: [SectionData<MainPageData>])
}

extension MainPagePresenter: MainPagePresenterProtocol {
   
    
}

extension MainPagePresenter: MainPageInteractorDelegate {
    func didLoadMainPage(screenSetup: [SectionData<MainPageData>]) {
        self.data = screenSetup
        delegate?.updateMainPage(data: self.data)
    }
}
