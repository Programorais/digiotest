//
//  MainPageRouter.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 10/11/20.
//

import Foundation

protocol MainPageRouterProtocol: BaseRouterProtocol {
}

final class MainPageRouter {
    class func createModule() -> MainPageViewController {
        let view = MainPageViewController()
        let presenter = MainPagePresenter(delegate: view, router: MainPageRouter(view: view))
        let interactor = MainPageInteractor(delegate: presenter, service: ScreenSetupService())
        presenter.setUp(interactor: interactor)
        view.setUp(presenter: presenter)
        return view
    }
    
    private let view: MainPageViewController
    
    init(view: MainPageViewController) {
        self.view = view
    }
    
}

extension MainPageRouter: BaseRouterProtocol {}
