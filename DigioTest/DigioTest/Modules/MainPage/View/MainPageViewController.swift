//
//  MainPageViewController.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 16/11/20.
//

import Foundation
import UIKit


final class MainPageViewController: BaseViewController<MainPagePresenterProtocol> {
  
    var sections: [SectionData<MainPageData>] = [SectionData<MainPageData>]() {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    lazy var tableView: UITableView = { return UITableView() }()
    lazy var navigationBar: UINavigationBar = { return UINavigationBar() }()

  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewCode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    
}

extension MainPageViewController: ViewCodeProtocol {
    
    func configureViews() {
        view.backgroundColor = .white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 300
        
        self.tableView.tableFooterView = UIView()
        self.tableView.register(CollectionTableViewCell.self,
                                forCellReuseIdentifier: "CollectionTableViewCell")
        self.tableView.alwaysBounceVertical = false
        self.tableView.separatorStyle = .none
        self.tableView.showsHorizontalScrollIndicator = false
    }
    
    func addSubviews() {
        self.view.addSubview(tableView)
        self.view.addSubview(navigationBar)
    }
    
    func setupConstraints() {
        self.setupConstraintsToBounds(view: tableView, parentView: self.view, paddingAround: 0)
    }
}

extension MainPageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let collectionCell = tableView.dequeueReusableCollectionTableViewCell(data: sections[indexPath.section].items,
                                                                                    cellType: CellType(rawValue: indexPath.section) ?? .cash,
                                                                                    indexPath: indexPath) else {
            return UITableViewCell()
        }
        collectionCell.contentView.isUserInteractionEnabled = true
        return collectionCell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        self.sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch CellType(rawValue: indexPath.section) {
        case .cash:
            return  100
        case .product:
            return  80
        case .spotlight:
            return  130
        case .none:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view.backgroundColor = .white
        let label = UILabel()
        view.addSubview(label)
        self.setupConstraintsToBounds(view: label, parentView: view, paddingAround: 20)
        label.attributedText = sections[section].title
        return view
    }
}

extension MainPageViewController: MainPagePresenterDelegate {
    func updateMainPage(data: [SectionData<MainPageData>]) {
        self.sections = data
    }
    
}
