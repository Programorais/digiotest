//
//  ScreenSetupService.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation
import UIKit

final class ScreenSetupService {

    
    func getScreenSetup(competion: @escaping (ScreenSetup?) -> ()) {
        let requester = RESTRequest(resource: ScreenSetupResource(querryItems: [], urlString: "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/", methodPath: "/sandbox/products"))
        requester.request { (result) in
            competion(result)
        }
    }
    
    func getImages(setup: ScreenSetup, completion: @escaping ([[UIImage]]) -> ()) {
        let dispatchGroup = DispatchGroup()
        
        var images = [[UIImage](), [UIImage](), [UIImage]()]
        
        
        setup.spotlight.forEach { (item) in
            dispatchGroup.enter()
            self.getImage(imageURL: item.bannerURL.absoluteString) { (image) in
                if let image = image {
                    images[0].append(image)
                } else {images[0].append(UIImage())}
                dispatchGroup.leave()
            }
        }
        
        
        dispatchGroup.enter()
        self.getImage(imageURL: setup.cash.bannerURL.absoluteString) { (image) in
            if let image = image {
                images[1].append(image)
            } else {images[1].append(UIImage())}
            dispatchGroup.leave()
        }
        
        
        setup.products.forEach { (item) in
            dispatchGroup.enter()
            self.getImage(imageURL: item.imageURL.absoluteString) { (image) in
                if let image = image {
                    images[2].append(image)
                } else {images[2].append(UIImage())}
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(images)
        }
    }

    
    func getImage(imageURL: String, completion: @escaping (UIImage?) -> ()){
        let requester = ImageRequest(url: URL(string: imageURL)!)
        requester.request { (result) in
            completion(result)
        }
    }
}

struct ScreenSetupResource: ApiResource {
    var querryItems: [URLQueryItem]
    var urlString: String
    typealias ModelType = ScreenSetup
    let methodPath: String
}
