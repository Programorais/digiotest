//
//  ScreenSetup.swift
//  TestDigio
//
//  Created by Vinicios de Morais on 12/11/20.
//

import Foundation

class ScreenSetup: Codable {
    
    let spotlight: [OptionSpotlightItem]
    let products: [OptionProductsItem]
    let cash: OptionCashItem
}

class OptionProductsItem: Codable {
    let name: String
    let imageURL: URL
    let description: String
}

class OptionCashItem: Codable {
    let bannerURL: URL
    let description: String
    let title: String
}

class OptionSpotlightItem: Codable {
    let bannerURL: URL
    let description: String
    let name: String
}
